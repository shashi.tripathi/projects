package com.us.jpa.practice;

import com.us.jpa.practice.dao.EmployeeDaoImpl;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class EmployeeServiceTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EmployeeServiceTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( EmployeeServiceTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	EmployeeDaoImpl dao=new EmployeeDaoImpl();
    	dao.createEmployee();
    	//System.out.println(dao.createEmployee());
    	
    }
}
