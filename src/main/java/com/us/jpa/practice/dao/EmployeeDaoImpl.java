package com.us.jpa.practice.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.us.jpa.practice.model.Employee;

public class EmployeeDaoImpl {

	public void createEmployee() {
		try {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("springjpa");
			EntityManager em = emf.createEntityManager();
			System.out.println("Em:" + em.isJoinedToTransaction());
			Employee emp = new Employee();
			emp.setId(1002);
			emp.setName("Shashi");
			emp.setSalary(5000);
		//	em.persist(emp);
			System.out.println("Persisted " + emp);
			Employee emp1 = em.find(Employee.class, 1002);
			System.out.println("Name:  " + emp1.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		new EmployeeDaoImpl().createEmployee();
	}

}
